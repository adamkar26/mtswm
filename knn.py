from ReliefF import ReliefF
import numpy as np
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_score, RepeatedKFold
from sklearn.feature_selection import SelectKBest, f_classif
import matplotlib
import matplotlib.pyplot as plt

def autolabel(rects):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in rects:
        height = round(rect.get_height(),2)
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')


if __name__ == '__main__':

    # wczytanie danych 
    data = pd.read_csv("./data.csv", sep=';', header=None).transpose().values
    target = pd.read_csv("./target.csv", sep=';', header=None).values[0]

    # dostępne metryki p =1 metryka manhatan, p=2 metryka euklidesowa
    metrics = [1, 2]
    k_list = [1, 5, 10]

    values = []

    for i in range(1, 11):
        # selekcja cech, n_features_to_keep - liczba cech do pozostawienia

        fs = ReliefF(n_neighbors=100, n_features_to_keep=i)
        X_trains = fs.fit_transform(data, target)
        values.append([])
        """
        # alterantywna selekcja cech
        fvalue_selector = SelectKBest(f_classif, k=i)
        X_trains = fvalue_selector.fit_transform(data, target)
        values.append([])
        """
        for k in k_list:
            values[i-1].append([])
            for p in metrics:
                # klasyfikator KNN
                knn = KNeighborsClassifier(n_neighbors=k, p=p, metric='minkowski')
                # 5 krotnie powtarzana 2-krotna walidacja krzyżowa
                rkf = RepeatedKFold(n_splits=2, n_repeats=5, random_state=2121)
                # obliczanie jakości klasygikacji
                scores = cross_val_score(estimator=knn, X=X_trains, y=target, scoring='accuracy', cv=rkf)
                score = np.mean(scores)
                values[i-1][-1].append(score)
                print('Wynik: %.3f +/- %.3f dla k = %d i p = %d, liczba cech %d'
                      % (np.mean(scores), np.std(scores), k, p, i))

    # generacja wykresów
    for i in range(0, 3):
        labels = [str(x+1) for x in range(len(values))]
        manhatan = [values[x][i][0] for x in range(len(values))]
        euklides = [values[x][i][1] for x in range(len(values))]

        x = np.arange(len(labels))  # the label locations
        width = 0.35  # the width of the bars

        fig_size = plt.rcParams["figure.figsize"]
        fig_size[0] = 12
        fig_size[1] = 8
        plt.rcParams["figure.figsize"] = fig_size
        # czcionka dla LaTeX
        plt.rc('text', usetex=True)
        plt.rc('font', family='serif')
        fig, ax = plt.subplots()
        rects1 = ax.bar(x - width / 2, manhatan, width, label='Manhatan')
        rects2 = ax.bar(x + width / 2, euklides, width, label='Euklides')

        # Add some text for labels, title and custom x-axis tick labels, etc.
        ax.set_ylabel('Dokładność')
        ax.set_xlabel('Liczba cech')
        ax.set_title('Wyniki dla k = %d' %(k_list[i]))
        ax.set_xticks(x)
        ax.set_xticklabels(labels)
        ax.legend()
        autolabel(rects1)
        autolabel(rects2)
        fig.tight_layout()
        plt.show()
        fig.savefig("./k"+str(k_list[i])+".pdf", bbox_inches='tight')


