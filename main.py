from ReliefF import ReliefF
import numpy as np
from sklearn import datasets
import pandas as pd

if __name__ == '__main__':

    data = pd.read_csv("./data.csv", sep=';', header=None).transpose().values
    target = pd.read_csv("./target.csv", sep=';', header=None).values[0]

    fs = ReliefF(n_neighbors=100)
    fs.fit(data, target)
    rank = fs.top_features
    scores = fs.feature_scores
    print(rank+1)
    print(scores)

    for i in rank:
        print(scores[i])
    
